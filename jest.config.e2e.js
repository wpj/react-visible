module.exports = {
  preset: 'jest-puppeteer',
  testMatch: ['**/*.spec.(js|jsx|ts|tsx)'],
};
